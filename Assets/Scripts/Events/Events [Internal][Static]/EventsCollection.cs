using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public static class EventsCollection 
{
    public static readonly GameEvent<string> OnAssetCreate = new GameEvent<string>();
    public static readonly GameEvent SO_Update = new GameEvent();

    #region InputEvents

    public static readonly GameEvent<InputAction.CallbackContext> OnJumpPressedInputEvent = new GameEvent<InputAction.CallbackContext>();
    public static readonly GameEvent<InputAction.CallbackContext> OnJumpReleased = new GameEvent<InputAction.CallbackContext>();
    public static readonly GameEvent<InputAction.CallbackContext> OnDash = new GameEvent<InputAction.CallbackContext>();
    public static readonly GameEvent<InputAction.CallbackContext> OnAttackInputPressed = new GameEvent<InputAction.CallbackContext>();

    public static readonly GameEvent OnAttackPerform = new GameEvent();
    public static readonly GameEvent<GameObject, List<GameObject>, int> DamageDealtFromOBJtoOBJ = new GameEvent<GameObject, List<GameObject>, int>();
    public static readonly GameEvent<GameObject> OnPlayerDamage = new GameEvent<GameObject>();

    public static readonly GameEvent<GameObject, EnemyState> EnemyStateEnter = new GameEvent<GameObject, EnemyState>();

    public static readonly GameEvent RuntimeDataUpdate = new GameEvent();
    public static readonly GameEvent OnCoinCollection = new GameEvent();

    public static readonly GameEvent OnEnemyDataUpdate = new GameEvent();
    public static readonly GameEvent<GameObject> OnZeroHP = new GameEvent<GameObject>();
    public static readonly GameEvent OnZeroHPPlayer = new GameEvent();

    public static readonly GameEvent OnGameOver = new GameEvent();

    public static readonly GameEvent<ActionState> ActionStateEnter = new GameEvent<ActionState>();
    public static readonly GameEvent<MovementState> MovementStateEnter = new GameEvent<MovementState>();

    public static readonly GameEvent<GameObject, int> OnEnemyDamageDealt = new GameEvent<GameObject, int>();



    #endregion

#if UNITY_EDITOR

    #region AssetEventsInvoke
    public class CustomAssetModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        static void OnWillCreateAsset(string assetName)
        {
            OnAssetCreate?.Invoke(assetName);
        }
        
    }

    #endregion

#endif
}
