using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface IGameEvent 
{
    public void Invoke();
    public void AddListener(Action listener);
    public void RemoveListener(Action listener);
}

public interface IGameEvent<T>
{
    public void Invoke(T value);
    public void AddListener(Action<T> listener);
    public void RemoveListener(Action<T> listener);
}

public interface IGameEvent<T, U>
{
    public void Invoke(T value1, U value2);
    public void AddListener(Action<T, U> listener);
    public void RemoveListener(Action<T, U> listener);
}

public interface IGameEvent<T, U, K>
{
    public void Invoke(T value1, U value2, K value3);
    public void AddListener(Action<T, U, K> listener);
    public void RemoveListener(Action<T, U, K> listener);
}