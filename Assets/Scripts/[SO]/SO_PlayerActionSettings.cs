using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerActionSettings", menuName = "PlayerInfo/ PlayerActionSettings")]
public class SO_PlayerActionSettings : ScriptableObject 
{
    [Header("Combat Statistics")]
    [Space(5)]
    [SerializeField] private float _attackDelayTime;
    [SerializeField] private float _immuneToDamageDelay;


    public float AttackDelayTime { get => _attackDelayTime; }
    public float ImmuneToDamageDelay { get => _immuneToDamageDelay; }

}
