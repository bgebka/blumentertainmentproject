using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerRuntimeData", menuName = "PlayerInfo/ PlayerRuntimeData")]
public class PlayerRuntimeData_SO : ScriptableObject
{
    [Header("PlayerStatistics")]
    [Space(5)]

    [SerializeField] private int _hp;
    [Space(5)]

    [Header("CollectedItems")]
    [SerializeField] private int _collectedCoins;

    [Space(5)]
    [Header("Monsters")]
    [SerializeField] private int _killedMonsters;

    [Space(5)]
    [Header("InMeleeRange")]
    [SerializeField] private List<GameObject> _objectsInMeleeRange;

    public int HP 
    { 
        get => _hp; 
        set 
        { 
            _hp = value;  
            EventsCollection.RuntimeDataUpdate?.Invoke(); 
            if(_hp <= 0)
            {
                _hp = 0;
                EventsCollection.OnZeroHPPlayer?.Invoke();
            }
        } 
    }
    public int CollectedCoins { get => _collectedCoins; set { _collectedCoins = value; EventsCollection.RuntimeDataUpdate?.Invoke(); } }
    public int KilledMonsters { get => _killedMonsters; set { _killedMonsters = value; EventsCollection.RuntimeDataUpdate?.Invoke(); } }

    public List<GameObject> ObjectsInMeleeRange { get => _objectsInMeleeRange; }

    bool IsIntialized => _objectsInMeleeRange != null ? true : false;
    private void Awake()
    {
        InitalizeCollections();
    }

    private void InitalizeCollections()
    {
        if (IsIntialized) return;

        _objectsInMeleeRange = new List<GameObject>();
    }
    private void OnValidate()
    {
        EventsCollection.RuntimeDataUpdate?.Invoke();
    }

    public void AddObjectToMeleeRangeList(GameObject meleeRangeObject)
    {
        if(!_objectsInMeleeRange.Contains(meleeRangeObject))
        {
            _objectsInMeleeRange.Add(meleeRangeObject);   
            EventsCollection.RuntimeDataUpdate?.Invoke();
        }
        else
        {
            Debug.LogWarning($"Object {meleeRangeObject} is already added to {_objectsInMeleeRange} list");
        }
    }
    public void RemoveObjectToMeleeRangeList(GameObject meleeRangeObject)
    {
        if (_objectsInMeleeRange.Contains(meleeRangeObject))
        {
            _objectsInMeleeRange.Remove(meleeRangeObject);
            EventsCollection.RuntimeDataUpdate?.Invoke();
        }
        else
        {
            Debug.LogWarning($"Object {meleeRangeObject} is not exist in {_objectsInMeleeRange} list");
        }
    }

}
