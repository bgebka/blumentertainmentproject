using UnityEngine;

[CreateAssetMenu(fileName = "PlayerDataSettings", menuName = "PlayerInfo/ PlayerDataSettings")]
public class PlayerDataSettings_SO : ScriptableObject
{
    [Header("PlayerStatistics")]
    [SerializeField] private GameObject _colliderObject;

    [Space(5)]
    [Header("PlayerStatistics")]
    [SerializeField] private int _hp;
    [SerializeField] private int _damage;

    [Space(5)]
    [Header("CollectedItems")]
    [SerializeField] private int _collectedCoins;

    [Space(5)]
    [Header("Monsters")]
    [SerializeField] private int _killedMonsters;


    public int HP { get => _hp;}
    public int CollectedCoins { get => _collectedCoins;}
    public int KilledMonsters { get => _killedMonsters;}
    public int Damage { get => _damage; }

    public GameObject ColliderGO { get => _colliderObject; }
}
