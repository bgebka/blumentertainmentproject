using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SO_EnemySettings : ScriptableObject, IEnemyData
{
    [SerializeField] private int _hp;
    [SerializeField] private bool _isJumping;
    [SerializeField] private bool _isFollowingPlayerOnAttack;

    public int HP { get => _hp; set => _hp = value; }
    public bool IsJumping { get => _isJumping; set => _isJumping = value; }
    public bool IsFollowingPlayerOnAttack { get => _isFollowingPlayerOnAttack; set => _isFollowingPlayerOnAttack = value; }
}
