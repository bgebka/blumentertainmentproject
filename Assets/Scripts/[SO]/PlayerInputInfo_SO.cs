﻿using UnityEngine;

[CreateAssetMenu(fileName = "Player Input Info", menuName = "PlayerInfo/ PlayerInputInfo")]
public class PlayerInputInfo_SO : ScriptableObject
{
    [SerializeField] private Vector2 _moveInput;
    public Vector2 MoveInput { get => _moveInput; set => _moveInput = value; }

}