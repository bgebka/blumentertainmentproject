using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MOTools
{
    public static T MyGetComponent<T>(GameObject gameObject) where T : Component
    {
        if (gameObject.TryGetComponent<T>(out var component))
        {
            return component;
        }
        else
        {
            Debug.LogError($"Object {gameObject} doesn't have required {typeof(T)} component");
            return null;
        }
    }
}
