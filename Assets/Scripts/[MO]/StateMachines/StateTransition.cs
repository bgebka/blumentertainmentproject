using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTransition
{
    private Type _startingPoint;
    private Type _endingPoint;


    public StateTransition() { }
    public StateTransition(Type startState, Type endState)
    {
        if (startState.GetType().IsClass && !startState.GetType().IsAbstract)
        {
            _startingPoint = startState.GetType();
            _endingPoint = endState.GetType();
        }

    }

    public StateTransition(BaseState startState, BaseState endState)
    {
        if (startState.GetType().IsClass && !startState.GetType().IsAbstract)
        {
            _startingPoint = startState.GetType();
            _endingPoint = endState.GetType();
        }

    }

    void GetTransitionParameters(out Type start, out Type end)
    {
        start = _startingPoint;
        end = _endingPoint;
    }

    public static bool operator ==(StateTransition a, StateTransition b)
    {
        if (a._startingPoint.GetType() == b._startingPoint.GetType() &&
            a._endingPoint.GetType() == b._endingPoint.GetType())
        {
            return true;
        }
        else return false;
    }

    public static bool operator !=(StateTransition a, StateTransition b)
    {
        if (a._startingPoint != b._startingPoint &&
            a._endingPoint != b._endingPoint)
        {
            return true;
        }
        else return false;
    }
}

public class StateTransition<T, F> : StateTransition
{
    private Type _startingPoint;
    private Type _endingPoint; 

    public StateTransition()
    {
        if (typeof(T).IsClass && !typeof(T).IsAbstract && typeof(F).IsClass && !typeof(F).IsAbstract)
        {
            _startingPoint = typeof(T);
            _endingPoint = typeof(F);
        }

    }

    public StateTransition(BaseState startState, BaseState endState)
    {
        if (startState.GetType().IsClass && !startState.GetType().IsAbstract)
        {
            _startingPoint = startState.GetType();
            _endingPoint = endState.GetType();
        }

    }

    public void GetTransitionTypes(out Type start, out Type end)
    {
        start = _startingPoint;
        end = _endingPoint;
    }

}