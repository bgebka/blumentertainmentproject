using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementState : BaseStateT<PlayerMovementStateMachine>
{
    protected PlayerMovementStateMachine StateMachine { get; private set; }

    protected MovementState(PlayerMovementStateMachine stateMachine) => StateMachine = stateMachine;
}
