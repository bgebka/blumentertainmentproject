
using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementStateMachine : BaseStateMachine
{
    [SerializeField] public SO_PlayerMovementSettings movementSettings;
    [SerializeField] public PlayerInputInfo_SO inputInfo;
    [SerializeField] public string currentStateSerialize;

    #region COMPONENTS
    public Rigidbody2D RB { get; private set; }
    public BoxCollider2D boxColider2D { get; private set; }

    [SerializeField] public float lastGroundTime;
    [SerializeField] public float lastPreesedJump;
    [SerializeField] public float lastPreeseeDash;

    #endregion

    #region STATE PARAMETERS
    public bool IsFacingRight { get; private set; }
    public float LastOnGroundTime { get; private set; }

    #endregion

    #region INPUT PARAMETERS
    public float LastPressedJumpTime { get; private set; }
    public float LastPressedDashTime { get; private set; }
    #endregion

    #region CHECK PARAMETERS
    [Header("Checks")]
    [SerializeField] float _groundCheckSize = 0.1f;

    #endregion

    #region LAYERS & TAGS

    [Header("Layers & Tags")]
    [SerializeField] private LayerMask _groundLayer;

    #endregion

    private void Awake()
    {
        InitalizeStateMachine<MovementState>(typeof(IdleMovementState));
        SetTransitions();


        EventHooksAdd();
        RB = GetComponent<Rigidbody2D>();
        boxColider2D = GetComponent<BoxCollider2D>();
    }

    private void OnDisable()
    {
        EventHooksRemove();
    }
    private void EventHooksAdd()
    {
        EventsCollection.OnJumpPressedInputEvent.AddListener(OnJump);
        EventsCollection.OnJumpReleased.AddListener(OnJumpRelease);
        EventsCollection.OnDash.AddListener(OnDashHandler);
    }

    private void EventHooksRemove()
    {
        EventsCollection.OnJumpPressedInputEvent.RemoveListener(OnJump);
        EventsCollection.OnJumpReleased.RemoveListener(OnJumpRelease);
        EventsCollection.OnDash.RemoveListener(OnDashHandler);
    }

    private void FixedUpdate()
    {
        currentStateSerialize = _currentState.ToString();
        _currentState.FixedUpdateState();
    }

    private void Update()
    {
        lastGroundTime = LastOnGroundTime;
        lastPreesedJump = LastPressedJumpTime;
        lastPreeseeDash = LastPressedDashTime;



        _currentState.UpdateState();

        LastOnGroundTime -= Time.deltaTime;
        LastPressedJumpTime -= Time.deltaTime;
        LastPressedDashTime -= Time.deltaTime;
        GroundCheck();
    }

    private void GroundCheck()
    {
        

        RaycastHit2D raycastHit = Physics2D.BoxCast(boxColider2D.bounds.center, new Vector2(boxColider2D.bounds.size.x - 0.1f, boxColider2D.bounds.size.y - 0.1f), 0f, Vector2.down, _groundCheckSize, _groundLayer);
        Color rayColor;
        if(raycastHit.collider != null)
        {
            rayColor = Color.green;
        } else
        {
            rayColor = Color.red;
        }

        Debug.DrawRay(boxColider2D.bounds.center + new Vector3(boxColider2D.bounds.extents.x, 0), Vector2.down * (boxColider2D.bounds.extents.y + _groundCheckSize), rayColor);
        Debug.DrawRay(boxColider2D.bounds.center - new Vector3(boxColider2D.bounds.extents.x, 0), Vector2.down * (boxColider2D.bounds.extents.y + _groundCheckSize), rayColor);
        Debug.DrawRay(boxColider2D.bounds.center - new Vector3(boxColider2D.bounds.extents.x, boxColider2D.bounds.extents.y + _groundCheckSize), Vector2.right * (boxColider2D.bounds.extents.x + _groundCheckSize), rayColor);

        if(raycastHit.collider != null) LastOnGroundTime = movementSettings.jumpDelayAfterGroundCheck; 
    }

    private void Start()
    {
        SetGravityScale(movementSettings.gravityScale);
        IsFacingRight = true;
    }
    private void OnDashHandler(InputAction.CallbackContext callbackContext)
    {
        LastPressedDashTime = movementSettings.dashBufferTime;
    }
    private void OnJump(InputAction.CallbackContext callbackContext)
    {
        LastPressedJumpTime = movementSettings.jumpBufferTime;
    }
    private void OnJumpRelease(InputAction.CallbackContext callbackContext)
    {
        if (GetState<JumpMovementState>().CanJumpCut())
            JumpCut();
    }

    public void SetGravityScale(float scale)
    {
        RB.gravityScale = scale;
    }

    public void SetTransitions()
    {
        SetAllTranistionsTo(true);
    }
    
    public void Drag(float amount)
    {
        Vector2 force = amount * RB.velocity.normalized;
        force.x = Mathf.Min(Mathf.Abs(RB.velocity.x), Mathf.Abs(force.x)); //ensures we only slow the player down, if the player is going really slowly we just apply a force stopping them
        force.y = Mathf.Min(Mathf.Abs(RB.velocity.y), Mathf.Abs(force.y));
        force.x *= Mathf.Sign(RB.velocity.x); //finds direction to apply force
        force.y *= Mathf.Sign(RB.velocity.y);

        RB.AddForce(-force, ForceMode2D.Impulse); //applies force against movement direction

      
    }

    public void CheckDirectionToFace(bool isMovingRight)
    {
        if (isMovingRight != IsFacingRight)
            Turn();
    }

    public void Turn()
    {
        Vector3 scale = transform.localScale; //stores scale and flips x axis, "flipping" the entire gameObject around. (could rotate the player instead)
        scale.x *= -1;
        transform.localScale = scale;

        IsFacingRight = !IsFacingRight;
    }
    public void Dash(Vector2 direction)
    {
        LastOnGroundTime = 0;
        LastPressedDashTime = 0;

        RB.velocity = direction.normalized * movementSettings.dashSpeed;

        SetGravityScale(0);
    }

    public void Jump() 
    {
        //ensures we can't call a jump multiple times from one press
        LastPressedJumpTime = 0;
        LastOnGroundTime = 0;

        #region Perform Jump
        float adjustedJumpForce = movementSettings.jumpForce;

        if (RB.velocity.y < 0)
            adjustedJumpForce -= RB.velocity.y;

        RB.AddForce(Vector2.up * adjustedJumpForce, ForceMode2D.Impulse);
        #endregion
    }

    public void JumpCut() 
    {
        //applies force downward when the jump button is released. Allowing the player to control jump height
        RB.AddForce(Vector2.down * RB.velocity.y * (1 - movementSettings.jumpCutMultiplier), ForceMode2D.Impulse);
    }

    public void Run(float amout)
    {
        float targetSpeed = inputInfo.MoveInput.x * movementSettings.runMaxSpeed; //calculate the direction we want to move in and our desired velocity
        float speedDif = targetSpeed - RB.velocity.x; //calculate difference between current velocity and desired velocity

        #region Acceleration Rate
        float accelRate;

        //gets an acceleration value based on if we are accelerating (includes turning) or trying to stop (decelerating). As well as applying a multiplier if we're air borne
        if (LastOnGroundTime > 0)
            accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? movementSettings.runAccel : movementSettings.runDeccel;
        else
            accelRate = (Mathf.Abs(targetSpeed) > 0.01f) ? movementSettings.runAccel * movementSettings.accelInAir : movementSettings.runDeccel * movementSettings.deccelInAir;

        //if we want to run but are already going faster than max run speed
        if (((RB.velocity.x > targetSpeed && targetSpeed > 0.01f) || (RB.velocity.x < targetSpeed && targetSpeed < -0.01f)) && movementSettings.doKeepRunMomentum)
        {
            accelRate = 0; //prevent any deceleration from happening, or in other words conserve are current momentum
        }
        #endregion

        #region Velocity Power
        float velPower;
        if (Mathf.Abs(targetSpeed) < 0.01f)
        {
            velPower = movementSettings.stopPower;
        }
        else if (Mathf.Abs(RB.velocity.x) > 0 && (Mathf.Sign(targetSpeed) != Mathf.Sign(RB.velocity.x)))
        {
            velPower = movementSettings.turnPower;
        }
        else
        {
            velPower = movementSettings.accelPower;
        }
        #endregion

        //applies acceleration to speed difference, then is raised to a set power so the acceleration increases with higher speeds, finally multiplies by sign to preserve direction
        float movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, velPower) * Mathf.Sign(speedDif);
        movement = Mathf.Lerp(RB.velocity.x, movement, amout);

        RB.AddForce(movement * Vector2.right); //applies force force to rigidbody, multiplying by Vector2.right so that it only affects X axis 

        if (inputInfo.MoveInput.x != 0)
            CheckDirectionToFace(inputInfo.MoveInput.x > 0);
    }

}
