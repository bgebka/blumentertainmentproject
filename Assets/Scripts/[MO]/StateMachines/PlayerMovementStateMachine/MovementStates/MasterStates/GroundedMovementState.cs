using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedMovementState : MovementState
{
    public GroundedMovementState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
        
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.MovementStateEnter?.Invoke(this);
        StateMachine.GetState<DashMovementState>().ResetDashes();
    }

    public override void Exit()
    {
    }

    public override void FixedUpdateState()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
        

        if (StateMachine.LastPressedDashTime > 0 && StateMachine.GetState<DashMovementState>().CanDash())
        {
            StateMachine.SwitchState(StateMachine.GetState<DashMovementState>());
        }
        else if (StateMachine.LastPressedJumpTime > 0)
        {
            StateMachine.SwitchState(StateMachine.GetState<JumpMovementState>());
        }
        else if (StateMachine.LastOnGroundTime <= 0)
        {
            StateMachine.SwitchState(StateMachine.GetState<InAirMovementState>());

        }
    }
}
