using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InAirMovementState : MovementState
{
    public InAirMovementState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.MovementStateEnter?.Invoke(this);
    }

    public override void Exit()
    {
        StateMachine.SetGravityScale(StateMachine.movementSettings.gravityScale);
    }

    public override void FixedUpdateState()
    {
        StateMachine.Drag(StateMachine.movementSettings.dragAmount);
        StateMachine.Run(1);
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
		if (StateMachine.LastPressedDashTime > 0 && StateMachine.GetState<DashMovementState>().CanDash())
		{
			StateMachine.SwitchState(StateMachine.GetState<DashMovementState>());
		}
		else if (StateMachine.LastOnGroundTime > 0)
		{
			StateMachine.SwitchState(StateMachine.GetState<IdleMovementState>());
		}
		else if (StateMachine.RB.velocity.y < 0)
		{
			//quick fall when holding down: feels responsive, adds some bonus depth with very little added complexity and great for speedrunners :D (In games such as Celeste and Katana ZERO)
			if (StateMachine.inputInfo.MoveInput.y < 0)
			{
				StateMachine.SetGravityScale(StateMachine.movementSettings.gravityScale * StateMachine.movementSettings.quickFallGravityMult);
			}
			else
			{
				StateMachine.SetGravityScale(StateMachine.movementSettings.gravityScale * StateMachine.movementSettings.fallGravityMult);
			}
		}

    }

}
