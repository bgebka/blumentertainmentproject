using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleMovementState : GroundedMovementState
{
    public IdleMovementState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        base.EnterState();
        EventsCollection.MovementStateEnter?.Invoke(this);
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void FixedUpdateState()
    {
        base.FixedUpdateState();
        StateMachine.Run(1);
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
        base.UpdateState();
        if(StateMachine.inputInfo.MoveInput.x != 0)
        {
            StateMachine.SwitchState(StateMachine.GetState<RunMovementState>());
        }
    }
}
