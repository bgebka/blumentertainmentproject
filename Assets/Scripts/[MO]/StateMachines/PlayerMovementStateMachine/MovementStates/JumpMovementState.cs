
public class JumpMovementState : GroundedMovementState
{
    public JumpMovementState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.MovementStateEnter?.Invoke(this);
        base.EnterState();
        StateMachine.Jump();
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override void Exit()
    {
        base.Exit();
    }
   
    public override void OnCollisionEnter()
    {
    }

    public override string ToString()
    {
        return base.ToString();
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (StateMachine.LastPressedDashTime > 0 && StateMachine.GetState<DashMovementState>().CanDash())
        {
            StateMachine.SwitchState(StateMachine.GetState<DashMovementState>());
        }
        else if (StateMachine.RB.velocity.y <= 0) //Jump performed, change state
        {
            StateMachine.SwitchState(StateMachine.GetState<InAirMovementState>());
        }
    }

    public bool CanJumpCut()
    {
        return (StateMachine.GetCurrentState() == StateMachine.GetState<InAirMovementState>() ||
            StateMachine.GetCurrentState() == StateMachine.GetState<JumpMovementState>()) && 
            StateMachine.RB.velocity.y > 0 
            
            ? true : false;
    }

    public override void FixedUpdateState()
    {
        base.FixedUpdateState();

        StateMachine.Drag(StateMachine.movementSettings.dragAmount);
        StateMachine.Run(1);
    }
}
