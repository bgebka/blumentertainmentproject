using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashMovementState : MovementState
{
    private Vector2 dir;
    private int dashesLeft;

    private bool dashAttacking;
    private float startTime;
    public DashMovementState(PlayerMovementStateMachine stateMachine) : base(stateMachine)
    {
    }


    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.MovementStateEnter?.Invoke(this);


        startTime = Time.time;

        dashesLeft--;

        dir = Vector2.zero; //get direction to dash in
        if (StateMachine.inputInfo.MoveInput == Vector2.zero)
            dir.x = (StateMachine.IsFacingRight) ? 1 : -1;
        else
            dir = StateMachine.inputInfo.MoveInput;

        dashAttacking = true;
        StateMachine.Dash(dir);
    }

    public override void Exit()
    {
        StateMachine.SetGravityScale(StateMachine.movementSettings.gravityScale);
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
        if (Time.time - startTime > StateMachine.movementSettings.dashAttackTime + StateMachine.movementSettings.dashEndTime) //dashTime over transition to another state
        {
            if (StateMachine.LastOnGroundTime > 0)
                StateMachine.SwitchState(StateMachine.GetState<IdleMovementState>());
            else
                StateMachine.SwitchState(StateMachine.GetState<InAirMovementState>());
        }
    }   

    private void StopDash()
    {
        dashAttacking = false;
        StateMachine.SetGravityScale(StateMachine.movementSettings.gravityScale);

        if (dir.y > 0)
        {
            if (dir.x == 0)
                StateMachine.RB.AddForce(Vector2.down * StateMachine.RB.velocity.y * (1 - StateMachine.movementSettings.dashUpEndMult), ForceMode2D.Impulse);
            else
                StateMachine.RB.AddForce(Vector2.down * StateMachine.RB.velocity.y * (1 - StateMachine.movementSettings.dashUpEndMult) * .7f, ForceMode2D.Impulse);
        }
    }

    public bool CanDash()
    {
        return dashesLeft > 0;
    }

    public void ResetDashes()
    {
        dashesLeft = StateMachine.movementSettings.dashAmount;
    }

    public override void FixedUpdateState()
    {
        if (Time.time - startTime > StateMachine.movementSettings.dashAttackTime)
        {
            //initial dash phase over, now begin slowing down and giving control back to player
            StateMachine.Drag(StateMachine.movementSettings.dragAmount);
            StateMachine.Run(StateMachine.movementSettings.dashEndRunLerp); //able to apply some run force but will be limited (~50% of normal)

            if (dashAttacking)
                StopDash();
        }
        else
        {
            StateMachine.Drag(StateMachine.movementSettings.dashAttackDragAmount);
        }
    }
}
