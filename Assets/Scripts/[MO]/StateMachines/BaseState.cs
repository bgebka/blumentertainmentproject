using System;
using UnityEngine;

public abstract class BaseState
{
    public abstract void ActiveTransitions();
    public abstract void DeactiveTransitions();
    public abstract void EnterState();
    public abstract void UpdateState();
    public abstract void FixedUpdateState();
    public abstract void OnCollisionEnter();
    public abstract void Exit();
}
