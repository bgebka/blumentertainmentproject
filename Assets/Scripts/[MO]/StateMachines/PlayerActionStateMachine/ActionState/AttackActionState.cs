public class AttackActionState : ActionState
{
    public AttackActionState(PlayerActionStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.ActionStateEnter?.Invoke(this);
    }

    public override void Exit()
    {
    }

    public override void FixedUpdateState()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
        if (StateMachine.ZeroHPTrigger)
        {
            StateMachine.SwitchState(StateMachine.GetState<DeadActionState>());
        }
        else if (StateMachine.LastDamageTakingTime > 0.0f)
        {
            StateMachine.SwitchState(StateMachine.GetState<DamageTakingActionState>());
        }
        else if(StateMachine.LastPressedAttackTime < 0.0f)
        {
            StateMachine.SwitchState(StateMachine.GetState<IdleActionState>());
        }
    }
}
