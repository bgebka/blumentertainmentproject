using UnityEngine;
public class DeadActionState : ActionState
{
    public DeadActionState(PlayerActionStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.ActionStateEnter?.Invoke(this);

        Debug.Log($"Good instance !: {StateMachine.name}");
    }

    public override void Exit()
    {
    }

    public override void FixedUpdateState()
    {
        
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
    }
}
