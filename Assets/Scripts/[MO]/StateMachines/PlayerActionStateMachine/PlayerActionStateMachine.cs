using System;
using UnityEngine.InputSystem;
using UnityEngine;
public class PlayerActionStateMachine : BaseStateMachine
{
    [SerializeField] private string _currentActionState;
    [SerializeField] private SO_PlayerActionSettings _actionSettings;

    [SerializeField] private float _lastPressedAttackTime = -0.1f;
    [SerializeField] private float _lastDamageTakingTime = -0.1f;
    private bool _attackAnimationDone = false;
    private bool _zeroHpTrigger = false;

    public float LastPressedAttackTime { get => _lastPressedAttackTime; }
    public float LastDamageTakingTime { get => _lastDamageTakingTime; set => _lastDamageTakingTime = value; }
    public bool AttackAnimationDone{ get => _attackAnimationDone; }
    public bool ZeroHPTrigger { get => _zeroHpTrigger; }

    private void Awake()
    {
        EventsCollection.OnAttackInputPressed.AddListener(OnAttackInputPressedHandling);
        EventsCollection.ActionStateEnter.AddListener(SerializeCurrentStateName);
        EventsCollection.OnZeroHPPlayer.AddListener(SetZeroHpTrigger);


        InitalizeStateMachine<ActionState>(typeof(IdleActionState));
        SetAllTranistionsTo(true);
    }

    private void OnDisable()
    {
        EventsCollection.OnAttackInputPressed.RemoveListener(OnAttackInputPressedHandling);
        EventsCollection.ActionStateEnter.RemoveListener(SerializeCurrentStateName);
        EventsCollection.OnZeroHPPlayer.RemoveListener(SetZeroHpTrigger);
    }

    private void SetZeroHpTrigger()
    {
        _zeroHpTrigger = true;
    }
    private void SerializeCurrentStateName(ActionState actionState)
    {
        _currentActionState = actionState.ToString();
    }
    void OnAttackInputPressedHandling(InputAction.CallbackContext callback)
    {
        if(_actionSettings is object)
        {
            _lastPressedAttackTime = _actionSettings.AttackDelayTime;
        }
    }
    public void OnAttackAnimationEndHandler()
    {
        _attackAnimationDone = true;
    }

    public void OnAttackAnimationStartHandler()
    {
        _attackAnimationDone = false;
    }
    private void FixedUpdate()
    {
        _currentState.FixedUpdateState();
    }

    private void Update()
    {
        _lastPressedAttackTime -= Time.deltaTime;
        _lastDamageTakingTime -= Time.deltaTime;

        _currentState.UpdateState();   
    }

}
