public abstract class ActionState : BaseStateT<PlayerActionStateMachine>
{
    protected PlayerActionStateMachine StateMachine { get;  set; }
    protected ActionState(PlayerActionStateMachine stateMachine) 
    {
        StateMachine = stateMachine;
    }
}
