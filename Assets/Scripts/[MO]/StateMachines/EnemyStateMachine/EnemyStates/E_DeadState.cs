using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_DeadState : EnemyState
{
    public E_DeadState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.EnemyStateEnter?.Invoke(StateMachine.gameObject, this);
    }

    public override void Exit()
    {
    }

    public override void FixedUpdateState()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
    }
}
