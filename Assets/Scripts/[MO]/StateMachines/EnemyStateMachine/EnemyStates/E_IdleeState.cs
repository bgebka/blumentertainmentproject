using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class E_IdleeState : EnemyState
{
    public E_IdleeState(EnemyStateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void ActiveTransitions()
    {
    }

    public override void DeactiveTransitions()
    {
    }

    public override void EnterState()
    {
        EventsCollection.EnemyStateEnter?.Invoke(StateMachine.gameObject, this);
    }

    public override void Exit()
    {
    }

    public override void FixedUpdateState()
    {
    }

    public override void OnCollisionEnter()
    {
    }

    public override void UpdateState()
    {
        if(StateMachine.IsDead)
        {
            StateMachine.SwitchState(StateMachine.GetState<E_DeadState>());
        } else if( StateMachine.LastDamageTakingTime > 0.0f)
        {
            StateMachine.SwitchState(StateMachine.GetState<E_DamageTakingState>());
        } else if (StateMachine.MeleeRange.InMeleeRange != null)
        {
            if (StateMachine.MeleeRange.InMeleeRange.Any(x => x?.gameObject.tag == "Player"))
            {
                StateMachine.SwitchState(StateMachine.GetState<E_AttackState>());
            }
        }
        if(StateMachine.AI.target != null)
        {
            if (StateMachine.AI.target.gameObject.tag == "Player")
            {
                StateMachine.SwitchState(StateMachine.GetState<E_PlayerFollowingState>());
            } else if(StateMachine.AI.target.gameObject.tag != "Player" && StateMachine.RB.velocity.sqrMagnitude > 0.1f)
            {
                StateMachine.SwitchState(StateMachine.GetState<E_PatrollingState>());
            }
        }
    }
}
