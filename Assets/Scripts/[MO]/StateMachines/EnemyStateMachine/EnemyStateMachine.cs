using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : BaseStateMachine
{
    [SerializeField] private string CurrentState;

    private EnemyAI _ai;
    private float _lastDamageTakingTime = - 0.1f;
    private bool _isDead = false;
    private Rigidbody2D _rb;
    private EnemyMeleeRangeManager _meleeRangeM;
    public float LastDamageTakingTime { get => _lastDamageTakingTime; }
    public bool IsDead { get => _isDead; }
    public EnemyAI AI { get => _ai; }
    public Rigidbody2D RB { get => _rb; }
    public EnemyMeleeRangeManager MeleeRange { get => _meleeRangeM; }

    private void GetRequiredComponents()
    {
        _ai = MOTools.MyGetComponent<EnemyAI>(this.gameObject);
        _rb = MOTools.MyGetComponent<Rigidbody2D>(this.gameObject);
        _meleeRangeM = MOTools.MyGetComponent<EnemyMeleeRangeManager>(this.gameObject);
    }
    private void EventsHookUp()
    {
        EventsCollection.OnZeroHP.AddListener(ZeroHPObjectHandling);
        EventsCollection.OnEnemyDamageDealt.AddListener(DamageTakingHandler);
    }
    private void EventsUnHook()
    {
        EventsCollection.OnZeroHP.RemoveListener(ZeroHPObjectHandling);
        EventsCollection.OnEnemyDamageDealt.RemoveListener(DamageTakingHandler);
    }

    private void ZeroHPObjectHandling(GameObject zeroHPobject)
    {
        if (this.gameObject == zeroHPobject)
        {
            _isDead = true;
        }
    }
    private void Awake()
    {
        EventsHookUp();
        InitalizeStateMachine<EnemyState>(typeof(E_IdleeState));
        SetAllTranistionsTo(true);
        GetRequiredComponents();
    }
    private void OnDisable()
    {
        EventsUnHook();
    }
    private void DamageTakingHandler(GameObject damageTaker, int damage)
    {
        if(damageTaker == this.gameObject)
        {
            _lastDamageTakingTime = 0.2f;
        }
    }
    private void FixedUpdate()
    {
        _currentState.FixedUpdateState();
    }

    public void UpdateTimers()
    {
        _lastDamageTakingTime -= Time.deltaTime;
    }

    private void Update()
    {
        UpdateTimers();
        _currentState.UpdateState();
        CurrentState = _currentState.ToString();
    }
}
