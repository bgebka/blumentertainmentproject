using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyState : BaseStateT<EnemyStateMachine>
{
    protected EnemyStateMachine StateMachine { get; private set; }
    protected EnemyState(EnemyStateMachine stateMachine) => StateMachine = stateMachine;
}
