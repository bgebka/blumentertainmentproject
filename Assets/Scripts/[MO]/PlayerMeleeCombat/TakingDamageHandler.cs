
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MashroomData))]
public class TakingDamageHandler : MonoBehaviour
{
    private MashroomData _data;
    private EnemyStateMachine _stateMachine;
    
    private void Awake()
    {
        _data = MOTools.MyGetComponent<MashroomData>(this.gameObject);
        _stateMachine = MOTools.MyGetComponent<EnemyStateMachine>(this.gameObject);

        EventsCollection.DamageDealtFromOBJtoOBJ.AddListener(TakingDamage);
    }

    private void OnDisable()
    {
        EventsCollection.DamageDealtFromOBJtoOBJ.RemoveListener(TakingDamage);
    }

    private void DeadEnemyHandler(GameObject zeroHPobject, EnemyState enterState)
    {
        if (_stateMachine.GetState<E_DeadState>() == enterState && zeroHPobject == this.gameObject)
        {
        }
    }

    private void TakingDamage(GameObject damageDealer, List<GameObject> MeleeRangeDeaelerObjects, int damageValue)
    {
        if(MeleeRangeDeaelerObjects.Contains(this.gameObject))
        {
            EventsCollection.OnEnemyDamageDealt.Invoke(this.gameObject, damageValue);
            _data.HP--;
        }
    }
}
