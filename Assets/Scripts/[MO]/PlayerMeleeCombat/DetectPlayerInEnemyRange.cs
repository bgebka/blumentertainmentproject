using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayerInEnemyRange : MonoBehaviour
{
    [SerializeField] private EnemyAttackHandler _attackHandler;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(_attackHandler is object)
            {
                _attackHandler.IsPlayerInRange = true;
                _attackHandler.Player = collision.gameObject;
            }
        }
    }
}
