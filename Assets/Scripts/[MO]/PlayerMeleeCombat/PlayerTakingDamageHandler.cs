using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTakingDamageHandler : MonoBehaviour
{
    private Rigidbody2D _rb2D;
    private PlayerActionStateMachine _playerActionSM;

    [SerializeField] private PlayerRuntimeData_SO _runtimeData;
    [SerializeField] private float _pushStrength = 0.5f;
    [SerializeField] private bool _isImmuneToDamage = false;

    [SerializeField] private SO_PlayerActionSettings _actionSettings;

    private float _immuneToDamageDelayTime = 2.0f;

    [SerializeField]
    private GameObject floatingTextPrefab;

    private void GetImmuneDelayFromSettings()
    {
        if(_actionSettings is object)
        {
            _immuneToDamageDelayTime = _actionSettings.ImmuneToDamageDelay;
        }
    }

    private void Awake()
    {
        GetComponentsAwake();
        GetImmuneDelayFromSettings();
        EventsCollection.OnPlayerDamage.AddListener(OnPlayerDamageHander);        
    }

    public void ShowDMGText()
    {
        if ( floatingTextPrefab is object)
        {
            Instantiate(floatingTextPrefab, transform.position, Quaternion.identity, transform);
        }
    }

    public void DiscableGameObjectOnEvent(GameObject gameObject)
    {
        if (gameObject == this.gameObject)
        {
            enabled = false;
        }
    }

    void GetComponentsAwake()
    {
        _playerActionSM = GetPlayerActionSMComponent();
        _rb2D = GetRigidBodyComponent();
    }
    private void OnDisable()
    {
        EventsCollection.OnPlayerDamage.RemoveListener(OnPlayerDamageHander);
    }
    private Rigidbody2D GetRigidBodyComponent()
    {
        if (TryGetComponent<Rigidbody2D>(out var rigidbody2D))
        {
            return rigidbody2D;
        }
        else
        {
            Debug.LogWarning("GameObject doesn't have Rigidbody2D component!");
            return null;
        }
    }

    private PlayerActionStateMachine GetPlayerActionSMComponent()
    {
        if (TryGetComponent<PlayerActionStateMachine>(out var playerActionSM))
        {
            return playerActionSM;
        }
        else
        {
            Debug.LogWarning($"{this.gameObject} doesn't have PlayerActionStateMachine component!");
            return null;
        }
    }

    private void OnPlayerDamageHander(GameObject enemyDamageDealer)
    {
        if(enemyDamageDealer.tag == "Enemy")
        {
            PushPlayerFromDamageDealer(enemyDamageDealer);   
        }
    }

    private void PushPlayerFromDamageDealer(GameObject enemyDamageDealer)
    {
        PushBackAfterDamage(enemyDamageDealer);

        if (!_isImmuneToDamage)
        {
            DamageDealingHandler();
        }
    }

    private void DamageDealingHandler()
    {
        ShowDMGText();
        _runtimeData.HP--;
        _playerActionSM.LastDamageTakingTime = 1f;
        _isImmuneToDamage = true;

        StartCoroutine(ImmuneDelayCounter());
    }

    private IEnumerator ImmuneDelayCounter()
    {
        yield return new WaitForSecondsRealtime(_immuneToDamageDelayTime);
        _isImmuneToDamage = false;
    }
    private void PushBackAfterDamage(GameObject enemyDamageDealer)
    {
        var direction = (this.gameObject.transform.position - enemyDamageDealer.transform.position).normalized;
        _rb2D.AddForce(new Vector2(direction.x * _pushStrength, direction.y * _pushStrength), ForceMode2D.Impulse);
    }
}
