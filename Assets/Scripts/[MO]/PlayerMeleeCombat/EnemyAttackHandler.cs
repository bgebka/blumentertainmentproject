using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyMeleeRangeManager), typeof(EnemyStateMachine), typeof(EnemyAI))]
public class EnemyAttackHandler : MonoBehaviour
{
    [SerializeField] private bool _isPlayerInRange = false;

    private GameObject _player;
    private EnemyAI _ai;
    private EnemyMeleeRangeManager _enemyMeleRange;

    public bool IsPlayerInRange { get => _isPlayerInRange; set =>_isPlayerInRange =  value; }
    public GameObject Player { get => _player; set => _player = value;  }

    private void Awake()
    {
        GetRequiredComponets();
    }

    private void Update()
    {
        FollowingPlayerOnRange();
    }
    private void GetRequiredComponets()
    {
        _ai = MOTools.MyGetComponent<EnemyAI>(gameObject);
        _enemyMeleRange = MOTools.MyGetComponent<EnemyMeleeRangeManager>(gameObject);

    }

    public void InvokePlayerDamageEvent()
    {
        if(_enemyMeleRange.InMeleeRange.Contains(Player))
        {
            EventsCollection.OnPlayerDamage?.Invoke(this.gameObject);
        }
    }
    private void FollowingPlayerOnRange()
    {
        if (_isPlayerInRange && Player is object)
        {
            _ai.PatrolPoints.Clear();
            _ai.target = _player.transform;
            _isPlayerInRange = false;
        }
    }
}
