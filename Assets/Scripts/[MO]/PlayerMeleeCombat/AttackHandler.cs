using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D), typeof(PlayerActionStateMachine))]
public class AttackHandler : MonoBehaviour
{
    private Rigidbody2D _rb;

    [SerializeField] private PlayerRuntimeData_SO runtimeData;
    [SerializeField] private PlayerDataSettings_SO settings;

    private PlayerActionStateMachine _playerActionSM;

    private PlayerActionStateMachine GetPlayerActionSM()
    {
        if(TryGetComponent<PlayerActionStateMachine>(out var playerActionSM))
        {
            return playerActionSM;
        }
        else
        {
            Debug.Log($"AttackHandler in gameobject: {this.gameObject} doesn't have {typeof(PlayerActionStateMachine)} component!");
            return null;
        }
    }

    private void GetRequirceComponents()
    {
        _playerActionSM = GetPlayerActionSM();
    }

    private void Awake()
    {
        GetRequirceComponents();
        GetRigidBodyComponent();
        EventsCollection.OnAttackPerform.AddListener(InvokeDamageDealtEvent);
    }

    private void OnDisable()
    {
        EventsCollection.OnAttackPerform.RemoveListener(InvokeDamageDealtEvent);
    }

    private void InvokeDamageDealtEvent()
    {
        if (_playerActionSM.GetCurrentState() == _playerActionSM.GetState<AttackActionState>())
        {
            if(runtimeData is object && settings is object)
            {
                SlowDownUnderAttack(0.05f);
                EventsCollection.DamageDealtFromOBJtoOBJ?.Invoke(this.gameObject, runtimeData.ObjectsInMeleeRange, settings.Damage);
            }
        }

    }
    private bool CheckGameObjecMeleeRangeList(GameObject hittedobject)
    {
        return runtimeData.ObjectsInMeleeRange.Contains(hittedobject);
    }
    private void GetRigidBodyComponent()
    {
        if (TryGetComponent<Rigidbody2D>(out var rigidbody2D))
        {
            _rb = rigidbody2D;
        }
    }
    
    private void SlowDownUnderAttack(float slowValueMultiplier)
    {
        _rb.velocity = new Vector2(_rb.velocity.x * slowValueMultiplier, _rb.velocity.y);
    }
}
