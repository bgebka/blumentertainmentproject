using UnityEngine;

public class PlayerMeleeRangeDetection : MonoBehaviour
{
    [SerializeField] private PlayerRuntimeData_SO _runtimeData;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _runtimeData.AddObjectToMeleeRangeList(collision.gameObject);

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        _runtimeData.RemoveObjectToMeleeRangeList(collision.gameObject);
    }

}
