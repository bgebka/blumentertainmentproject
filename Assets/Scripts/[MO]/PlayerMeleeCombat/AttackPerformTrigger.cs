using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPerformTrigger : MonoBehaviour
{
    public void TriggerAttackEvent()
    {
        EventsCollection.OnAttackPerform?.Invoke();
    }
}
