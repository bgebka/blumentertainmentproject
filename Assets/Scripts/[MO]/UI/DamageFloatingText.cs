using System.Collections.Generic;
using UnityEngine;

public class DamageFloatingText : MonoBehaviour
{
    [SerializeField]
    private GameObject floatingTextPrefab;

    private void Awake()
    {
        EventHook();
    }

    private void OnDisable()
    {
        EventUnHook();
    }
    private void EventHook()
    {
        EventsCollection.DamageDealtFromOBJtoOBJ.AddListener(ShowDMGText);
       
    }

    private void EventUnHook()
    {
        EventsCollection.DamageDealtFromOBJtoOBJ.RemoveListener(ShowDMGText);
        
    }

    public void ShowDMGText(GameObject damageDealer,List<GameObject> damageTakerList, int dmg)
    {
        if(damageTakerList.Contains(this.gameObject) && floatingTextPrefab is object)
        {
            Instantiate(floatingTextPrefab, transform.position, Quaternion.identity, transform);
        }
    }

    public void DiscableGameObjectOnEvent(GameObject gameObject)
    {
        if(gameObject == this.gameObject)
        {
            enabled = false;
        }
    }

}
