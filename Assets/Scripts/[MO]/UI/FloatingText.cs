using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    [SerializeField] private AnimationCurve _curve;
    [SerializeField] private float _destroyTime = 1.0f;
    [SerializeField] private float _randomXPositionRange = 2.0f;
    [SerializeField] private float _randomYPositionRange = 2.0f;
    [SerializeField] private float _startingScaleValue = 0.1f;
    [SerializeField] private float _targetScaleSetValue = 1.0f;

    [SerializeField] private float _randomFloatingSpeedRange = 2.0f;
    [SerializeField] private PlayerDataSettings_SO playerData;

    private float risingSpeed = 9.0f;
    private float fallignSpeed = 3.0f;
    private float curveSpeed;

    bool isFullScaled = false;

    private float _randomSpeed;
    private Vector3 _scale;
    private float _evalateValue;
    private Vector3 _targetScale;
    private Vector3 _startingScale;
    private float _currentDamage;


    private GameObject MainCamera;
    private TextMeshPro TextMeshPro;

    private void Awake()
    {
        TextMeshPro = GetComponent<TextMeshPro>();
        MainCamera = GameObject.Find("MainCamera");


        _evalateValue = 0.0f;
        SetStartingScale();
        _randomSpeed = SetRandomSpeed();
        ChangePositionOnStart();
        StartCoroutine(DestroyObjectAfterDeylay());
    }
    
    private void Update()
    {
        if(playerData is object)
        {
            _currentDamage = playerData.Damage;
        }
        TextMeshPro.text = string.Format($"-{(int)_currentDamage}");


        transform.Translate(Vector3.up * Time.deltaTime * _randomSpeed);

        _evalateValue = Mathf.Lerp(_evalateValue, 1.0f, Time.deltaTime * curveSpeed);

       
        if(!isFullScaled)
        {
            _scale = Vector3.Lerp(_startingScale, _targetScale, _curve.Evaluate(_evalateValue));
        }
        else
        {
            _scale = Vector3.Lerp(_targetScale, _startingScale, _curve.Evaluate(_evalateValue));
        }

        transform.localScale = _scale;

        if (_evalateValue > 0.95f)
        {
            _evalateValue = 0.0f;
            isFullScaled = true;
            curveSpeed = fallignSpeed;
        }
    }
    
    private void SetStartingScale()
    {
        _scale = new Vector3(_startingScaleValue, _startingScaleValue, _startingScaleValue);
        _targetScale = new Vector3(_targetScaleSetValue, _targetScaleSetValue, _targetScaleSetValue);
        _startingScale = new Vector3(_startingScaleValue, _startingScaleValue, _startingScaleValue);

        curveSpeed = risingSpeed;
    }
    private float SetRandomSpeed()
    {
        return Random.Range(1.0f, _randomFloatingSpeedRange);
    }
    private void ChangePositionOnStart()
    {
        transform.Translate(transform.localPosition.x + Random.Range(-_randomXPositionRange, _randomXPositionRange),
                            transform.localPosition.y + Random.Range(-_randomYPositionRange, _randomXPositionRange),
                            transform.localPosition.z, 
                            Space.World);


        Vector3 rotationToPlayer = MainCamera.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(rotationToPlayer, Vector3.up);
        transform.rotation = rotation;
        transform.rotation = Quaternion.Euler(0.0f, transform.rotation.eulerAngles.y + 180.0f, 0.0f);
    }
    public IEnumerator DestroyObjectAfterDeylay()
    {
        yield return new WaitForSecondsRealtime(_destroyTime);
        DestroyImmediate(this.gameObject);
    }
}
