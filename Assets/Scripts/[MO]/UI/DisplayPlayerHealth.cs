using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class DisplayPlayerHealth : MonoBehaviour
{
    [SerializeField] private PlayerDataSettings_SO _startingData;
    [SerializeField] private PlayerRuntimeData_SO _runtimeData;
    [SerializeField] private TextMeshProUGUI _heathText;
    [SerializeField] private GameObject _healthSprite;
    
    private float _heathFillAmount = 1.0f;
    private Image _healthImage;

    private void Start()
    {
        AssigneDataValue();
    }

    private void GetHealthSpriteComponent()
    {
        if(_healthSprite is object)
        {
            if (_healthSprite.TryGetComponent<Image>(out var healthImage))
            {
                _healthImage = healthImage;
            }
            else
            {
                Debug.LogError($"HealthSprite Object doesn't have {typeof(Image)} component! ");
            }
        }
        else
        {
            Debug.LogError($"GameObject with HealthSprite is not assigne to {gameObject}!");
        }
    }
    private void Awake()
    {
        GetHealthSpriteComponent();
        EventsCollection.RuntimeDataUpdate.AddListener(AssigneDataValue);
    }

    private void OnDisable()
    {
        EventsCollection.RuntimeDataUpdate.RemoveListener(AssigneDataValue);
    }

    private void AssigneDataValue()
    {
        if(_heathText is object && _runtimeData is object)
        {
            _heathText.text = string.Format($"{_runtimeData.HP}");
        }

        ChangeHeartFillAmount();
    }

    private void ChangeHeartFillAmount()
    {
        if(_healthImage is object && _startingData.HP != 0)
        {
            var fillValue = ((float)_runtimeData.HP / (float)_startingData.HP);
            _healthImage.fillAmount = fillValue;
        }
    }
}
