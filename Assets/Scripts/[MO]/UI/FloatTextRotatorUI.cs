using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatTextRotatorUI : MonoBehaviour
{
    private Rigidbody2D _rb;

    public void Update()
    {
        if(_rb.velocity.x >= 0.0f)
        {
            transform.localScale = new Vector3(-1f * Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
    }
    public void Start()
    {
        GetRigidBody();
    }
    public void GetRigidBody()
    {
        _rb =  transform.root.GetComponent<Rigidbody2D>();
    }
}
