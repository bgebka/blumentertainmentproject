using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveResetButtonUI : MonoBehaviour
{
    [SerializeField] GameObject Button;
    [SerializeField] GameObject GameOverText;

    public void TurnOnAnotherEndingObject()
    {
        Button.SetActive(true);
        GameOverText.SetActive(true);
    }
}
