using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DisplayCollectedCoins : MonoBehaviour
{
    [SerializeField] private PlayerRuntimeData_SO _runtimeData;
    [SerializeField] private TextMeshProUGUI _coinText;

    private void Awake()
    {
        EventsCollection.RuntimeDataUpdate.AddListener(AssigneDataValue);
    }

    private void OnDisable()
    {
        EventsCollection.RuntimeDataUpdate.RemoveListener(AssigneDataValue);
    }


    private void AssigneDataValue()
    {
        if (_coinText is object && _runtimeData is object)
        {
            _coinText.text = string.Format($"{_runtimeData.CollectedCoins}");
        }

    }
}
