using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TurnOnGameOverPanelUI : MonoBehaviour
{
    [SerializeField] private GameObject _gameOverPanel;
    private void Awake()
    {
        EventsCollection.OnGameOver.AddListener(TurnOnPanel);
    }

    private void OnDisable()
    {
        EventsCollection.OnGameOver.RemoveListener(TurnOnPanel);
    }

    private void TurnOnPanel()
    {
        _gameOverPanel.SetActive(true);
    }
}
