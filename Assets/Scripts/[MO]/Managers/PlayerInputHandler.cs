using UnityEngine;
public class PlayerInputHandler : MonoBehaviour
{
    [SerializeField] private PlayerInputInfo_SO _playerInputInfo;

    private PlayerInputActions _inputActions;

    private void Awake()
    {
        _inputActions = new PlayerInputActions();
        PlayerInputHooks();
    }

    private void PlayerInputHooks()
    {
        if (_inputActions is not object) _inputActions = new PlayerInputActions();

        _inputActions.PlayerActionMap.Jump.performed += x => EventsCollection.OnJumpPressedInputEvent?.Invoke(x);
        _inputActions.PlayerActionMap.JumpRelease.performed += x => EventsCollection.OnJumpReleased?.Invoke(x);
        _inputActions.PlayerActionMap.Dash.performed += x => EventsCollection.OnDash?.Invoke(x);
        _inputActions.PlayerActionMap.AttackNew.performed += x => EventsCollection.OnAttackInputPressed?.Invoke(x);

    }

    private void FixedUpdate()
    {
        _playerInputInfo.MoveInput = _inputActions.PlayerActionMap.Movement.ReadValue<Vector2>();
    }
    private void OnEnable()
    {
        _inputActions.Enable();
    }

    private void OnDisable()
    {
        _inputActions.Disable();
    }
}
