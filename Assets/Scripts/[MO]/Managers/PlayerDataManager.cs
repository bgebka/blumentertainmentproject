using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager : MonoBehaviour
{
    [SerializeField] private PlayerDataSettings_SO startingData;
    [SerializeField] private PlayerRuntimeData_SO runtimeData;

    private void Start()
    {
        AsignRunTimeData(startingData);
    }

    private void Awake()
    {
        EventsCollection.OnCoinCollection.AddListener(CoinCollectionHandler);
    }

    private void OnDisable()
    {
        EventsCollection.OnCoinCollection.RemoveListener(CoinCollectionHandler);
    }
    private void AsignRunTimeData(PlayerDataSettings_SO startingData)
    {
        if(runtimeData is object && startingData is object)
        {
            runtimeData.HP = startingData.HP;
            runtimeData.CollectedCoins = startingData.CollectedCoins;
            runtimeData.KilledMonsters = startingData.KilledMonsters;
        }
        else
        {
            Debug.LogError($"PlayerData is not asign to {gameObject}!");
        }
    }

    private void CoinCollectionHandler()
    {
        if(runtimeData is object)
        {
            runtimeData.CollectedCoins++;
        }
        else
        {
            Debug.LogError($"DataManager doesn't have assigned {typeof(PlayerRuntimeData_SO)}");
        }
    }
}
