using UnityEngine;
using UnityEngine.InputSystem;

public class AnimationStateHandler : MonoBehaviour
{
    PlayerMovementStateMachine playerMovementStateMachine;
    PlayerActionStateMachine playerActionStateMachine;
    Animator animator;
    Rigidbody2D RB;

    bool IsJumping = false;
    private void Awake()
    {
        playerActionStateMachine = MOTools.MyGetComponent<PlayerActionStateMachine>(gameObject);
        RB = GetComponent<Rigidbody2D>();
        playerMovementStateMachine = GetComponent<PlayerMovementStateMachine>();
        animator = GetComponent<Animator>();

        EventsCollection.ActionStateEnter.AddListener(ChangeAnimationsParametersState);
        EventsCollection.MovementStateEnter.AddListener(ChangeAnimationsParametersState);
        EventsCollection.OnAttackInputPressed.AddListener(ChangeAttackState);
    }

    private void OnDisable()
    {
        EventsCollection.MovementStateEnter.RemoveListener(ChangeAnimationsParametersState);
        EventsCollection.ActionStateEnter.RemoveListener(ChangeAnimationsParametersState);
        EventsCollection.OnAttackInputPressed.RemoveListener(ChangeAttackState);
    }


    private void FixedUpdate()
    {
        if (IsJumping)
        {
            animator.SetFloat("VelocityY", RB.velocity.y);
        }
    }
    private void ChangeAttackState(InputAction.CallbackContext callback)
    {
        animator.SetBool("Attack", true);
    }

    public void ResetAttackState()
    {
        animator.SetBool("Attack", false);
    }

    private void ChangeAnimationsParametersState(BaseState enterState)
    {
        animator.ResetTrigger("Idle");
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Dash");
        animator.SetBool("JumpAndAir", false);
        animator.SetBool("DamageT", false);
        IsJumping = false;

        if (enterState == playerMovementStateMachine.GetState<IdleMovementState>())
        {
            animator.SetTrigger("Idle");
        }
        if (enterState == playerActionStateMachine.GetState<DamageTakingActionState>())
        {
            animator.SetBool("DamageT", true);
        }
        if (enterState == playerMovementStateMachine.GetState<RunMovementState>())
        {
            animator.SetTrigger("Run");
        }
        if (enterState == playerMovementStateMachine.GetState<IdleMovementState>())
        {
            animator.SetTrigger("Idle");
        }
        if (enterState == playerMovementStateMachine.GetState<DashMovementState>())
        {
            animator.SetTrigger("Dash");
        }
        if (enterState == playerMovementStateMachine.GetState<JumpMovementState>())
        {
            animator.SetBool("JumpAndAir", true);
            IsJumping = true;
        }

    }
}
