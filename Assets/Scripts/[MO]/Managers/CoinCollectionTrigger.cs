using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollectionTrigger : MonoBehaviour
{
    [SerializeField] private PlayerDataSettings_SO playerSettings;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            EventsCollection.OnCoinCollection?.Invoke();
            Destroy(this.gameObject);
        }
    }
}
