using UnityEngine;

public abstract class DeathHandler : MonoBehaviour
{
    protected bool deadTrigger = false;
    protected Rigidbody2D _rb;
    protected Animator _animator;

    protected virtual void GetRequireComponents()
    {
        _animator = MOTools.MyGetComponent<Animator>(gameObject);
        _rb = MOTools.MyGetComponent<Rigidbody2D>(gameObject);
    }

    protected abstract void DeadPlayerExecute();
    protected abstract void PlayDeadAnimation();
    protected abstract void RemoveMovement();
 
}
