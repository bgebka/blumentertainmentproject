using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathHandler : DeathHandler
{
    private EnemyStateMachine _stateMachine;
    private DamageFloatingText _floatingText;
    private bool _isDead = false;

    [SerializeField] private GameObject _leftCollider;
    [SerializeField] private GameObject _rightCollider;
    [SerializeField] private GameObject _topCollider;

    private CapsuleCollider2D _mainCollider;
    
    [SerializeField] private string DeadAnimationText;

    private void Awake()
    {
        EventsHookUp();
        ComponentsCollection();
    }

    private void EventsHookUp()
    {
        EventsCollection.EnemyStateEnter.AddListener(DeadEnemyHandler);
    }

    private void EventsUnHook()
    {
        EventsCollection.EnemyStateEnter.RemoveListener(DeadEnemyHandler);
    }
    private void ComponentsCollection()
    {
        GetRequireComponents();
        _floatingText = MOTools.MyGetComponent<DamageFloatingText>(this.gameObject);
        _stateMachine = MOTools.MyGetComponent<EnemyStateMachine>(this.gameObject);
        _mainCollider = MOTools.MyGetComponent<CapsuleCollider2D>(this.gameObject);
    }

    private void OnDisable()
    {
        EventsUnHook();
    }

    private void DeadEnemyHandler(GameObject zeroHPobject,EnemyState enterState)
    {
        if(_stateMachine.GetState<E_DeadState>() == enterState && zeroHPobject == this.gameObject)
        {
            _isDead = true;
        }
    }
   
    private void FixedUpdate()
    {
        DeadPlayerExecute();
    }

    public void ResetAnimationEndingFlag()
    {
        Destroy(this.gameObject);
    }
    protected override void DeadPlayerExecute()
    {
        if (_isDead)
        {
            TurnOffFloatingText();
            RemoveMovement();
            DestroyColliders();
            PlayDeadAnimation();
        }
    }
    private void TurnOffFloatingText()
    {
        _floatingText.enabled = false;
    }
    protected override void PlayDeadAnimation()
    {
        _animator.Play(DeadAnimationText, 0);
    }

    protected override void RemoveMovement()
    {
        _rb.velocity = new Vector2(0.0f, 0.0f);
    }
    private void DestroyColliders()
    {
        if(_leftCollider is object &&
            _rightCollider is object &&
            _mainCollider is object &&
            _topCollider is object)
        {
            Destroy(_leftCollider);
            Destroy(_rightCollider);
            Destroy(_topCollider);
            _mainCollider.enabled = false;
        }
    }
}
