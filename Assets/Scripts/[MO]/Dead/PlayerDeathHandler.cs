using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public sealed class PlayerDeathHandler : DeathHandler
{
    private PlayerActionStateMachine _actionSM;

    
    private void Awake()
    {
        _actionSM = MOTools.MyGetComponent<PlayerActionStateMachine>(this.gameObject);
        GetRequireComponents();
        EventsCollection.ActionStateEnter.AddListener(DeathHandling);
    }

    
    private void OnDisable()
    {
        EventsCollection.ActionStateEnter.RemoveListener(DeathHandling);
    }

    private void DeathHandling(ActionState actionState)
    {
        if (actionState == _actionSM.GetState<DeadActionState>())
        {
            deadTrigger = true;
        }
    }

    private void Update()
    {
        DeadPlayerExecute();
    }
    protected override void DeadPlayerExecute()
    {
        if(deadTrigger)
        {
            RemoveMovement();
            PlayDeadAnimation();
            EventsCollection.OnGameOver.Invoke();
        }
    }

    protected override void PlayDeadAnimation()
    {
        if(_animator is object)
        {
            _animator.Play("PlayerAnimation_Death", 0);
        }
    }
    protected override void RemoveMovement()
    {
        _rb.simulated = false;
    }
}
