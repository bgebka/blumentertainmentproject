using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MashroomAnimationManager : MonoBehaviour
{
    private EnemyStateMachine _stateMachine;
    private Animator _animator;
    private Rigidbody2D _rb;

    private void GetRequiredComponents()
    {
        _rb = MOTools.MyGetComponent<Rigidbody2D>(this.gameObject);
        _stateMachine = MOTools.MyGetComponent<EnemyStateMachine>(this.gameObject);
        _animator = MOTools.MyGetComponent<Animator>(this.gameObject);
    }

    private void Awake()
    {
        EventsHookUp();
        GetRequiredComponents();
    }

    private void Start()
    {
        AnimationStateChangeHandling(this.gameObject, _stateMachine.GetCurrentState() as EnemyState);
    }

    private void OnDisable()
    {
        EventsUnHook();
    }
    private void OnDestroy()
    {
        EventsUnHook();
    }
    private void EventsHookUp()
    {
        EventsCollection.EnemyStateEnter.AddListener(AnimationStateChangeHandling);
    }

    private void EventsUnHook()
    {
        EventsCollection.EnemyStateEnter.RemoveListener(AnimationStateChangeHandling);
    }

    private void AnimationStateChangeHandling(GameObject gameObject, EnemyState state)
    {
        if(gameObject == this.gameObject)
        {
            AnimatorStateChange(state);
        }
    }

    private void AnimatorStateChange(EnemyState state)
    {
        _animator.SetBool("Walking", false);
        _animator.SetBool("Idlee", false);
        _animator.SetBool("DamageT", false);

        if (IsStateMatch<E_AttackState>(state))
        {

        }
        if (IsStateMatch<E_IdleeState>(state))
        {
            _animator.SetBool("Idlee", true);
        }
        if (IsStateMatch<E_PatrollingState>(state) || IsStateMatch<E_PlayerFollowingState>(state))
        {
            _animator.SetBool("Walking", true);
        }
        if (IsStateMatch<E_DamageTakingState>(state))
        {
            _animator.SetBool("DamageT", true);
        }

    }

    private bool IsStateMatch<T>(EnemyState state) where T: EnemyState
    {
        return _stateMachine.GetState<T>() == state;
    }
}
