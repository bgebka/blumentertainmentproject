using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyData 
{
    public int HP { get; set; }

    #region AI 
    public bool IsJumping { get; set; }
    public bool IsFollowingPlayerOnAttack { get; set; }

    #endregion

}
