using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MashroomData : MonoBehaviour
{
    [SerializeField] private int _hp;
    [SerializeField] private bool _isJumping;
    [SerializeField] private bool _isFollowingPlayerOnAttack;

    [SerializeField] MashroomSettings _mashroomSettings;
    public int HP
    {
        get => _hp;
        set
        {
            _hp = value;
            EventsCollection.OnEnemyDataUpdate.Invoke();

            if (_hp <= 0)
            {
                _hp = 0;
                EventsCollection.OnZeroHP?.Invoke(this.gameObject);
            }
        }
    }

    public bool IsJumping
    {
        get => _isJumping;
        set
        {
            _isJumping = value;
        }
    }

    public bool IsFollowingPlayerOnAttack
    {
        get => _isFollowingPlayerOnAttack;
        set
        {
            _isFollowingPlayerOnAttack = value;
            EventsCollection.OnEnemyDataUpdate.Invoke();
        }
    }

    private void AssigneStartingParameterFromSetting()
    {
       if(_mashroomSettings is object)
        {
            HP = _mashroomSettings.HP;
            IsJumping = _mashroomSettings.IsJumping;
            IsFollowingPlayerOnAttack = _mashroomSettings.IsFollowingPlayerOnAttack;

        }
    }

    private void Start()
    {
        AssigneStartingParameterFromSetting();
    }
   
}
