using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeRangeManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> _inMeleeRange;
    public List<GameObject> InMeleeRange { get => _inMeleeRange; }
    bool IsInitalized => _inMeleeRange != null ? true : false;
    private void InitalizeCollections()
    {
        if (IsInitalized) return;

        _inMeleeRange = new List<GameObject>();
    }

    private void Awake()
    {
        InitalizeCollections();
    }
    public void AddObjectToMeleeRangeList(GameObject detectedObject)
    {
        InitalizeCollections();

        if(!_inMeleeRange.Contains(detectedObject))
        {
            _inMeleeRange.Add(detectedObject);
        }
        else
        {
            Debug.LogWarning($"Object : {detectedObject} already is in {_inMeleeRange} List");
        }
    }

    public void RemoveObjectToMeleeRangeList(GameObject detectedObject)
    {
        InitalizeCollections();

        if (_inMeleeRange.Contains(detectedObject))
        {
            _inMeleeRange.Remove(detectedObject);
        }
        else
        {
            Debug.LogWarning($"Object {detectedObject} doesn't exist in {_inMeleeRange} List");
        }
    }
}
