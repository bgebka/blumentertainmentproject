using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomDamageDealer : MonoBehaviour
{
    private BoxCollider2D _boxCollider2D;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            EventsCollection.OnPlayerDamage?.Invoke(transform.root.gameObject);
        }
    }

}
