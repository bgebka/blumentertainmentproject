using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpOnTheHeadDamageHandler : MonoBehaviour
{
    [Header("Physics")]
    
    [SerializeField] float jumpingForce = 30.0f;

    private Animator _animator;
    private string _crushedAnimation = "MashRoom_Crushed";

    private void GetAnimatorFromParent()
    {
        if(transform.parent.TryGetComponent<Animator>(out var animator))
        {
            _animator = animator;
        }
        else
        {
            Debug.LogError($"Parent object {transform.parent} doesn't have required {typeof(Animator)} component!");
        }
    }
    private void Awake()
    {
        GetAnimatorFromParent();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        CollisionHandling(collision);
    }

    private void CollisionHandling(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            var RB = collision.gameObject.GetComponent<Rigidbody2D>();
            if (RB is object)
            {
                PlayerCrushedAnimation();
                AddForceAfterCrushedMushRoom(RB);
            }
        }
    }

    private void AddForceAfterCrushedMushRoom(Rigidbody2D RB)
    {
        RB.AddForce(Vector2.up * jumpingForce, ForceMode2D.Impulse);
    }

    private void PlayerCrushedAnimation()
    {
        _animator.Play(_crushedAnimation, 0);
    }
}
