using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class EnemyMeleeRangeDetector : MonoBehaviour
{
    [SerializeField] private EnemyMeleeRangeManager _meleeRange;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(_meleeRange is object)
        {
            _meleeRange.AddObjectToMeleeRangeList(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(_meleeRange is object)
        {
            _meleeRange.RemoveObjectToMeleeRangeList(collision.gameObject);
        }
    }
}
